Currency Notify
===============

This is a simple script to generate Unity notifications based on currency exchange rates.

Dependencies
------------

* Python 3.7+

* notify-send unity package
  ::
    apt install notify-send

* Currency layer API key
  ::
    https://currencylayer.com

Usage
-----

* Install requirements
  ::
    pip install -r requirements.txt

* Set API_LAYER_KEY environment variable
  ::
    export API_LAYER_KEY=<your key>

* Run script, passing "from" and "to" currencies
  ::
    python currency_notify.py <from> <to>

    # for Euro to Brazilian Real

    python currency_notify.py EUR BRL

